<?php
// $Id: indavideo.inc.txt,v 1.1 2010/10/20 18:45:11 alexua Exp $

/**
 * @file
 *  This is an indavideo provider include file for Embedded Media Video.
 *  Use this as a base for creating new provider files.
 *
 *  When using this, first make the following global replacements:
 *    * Replace INDAVIDEO with the name of your provider in all caps.
 *    * Replace indavideo with the name of your provider in all lower case.
 *    * Replace Indavideo with the name (to be translated) of your provider in
 *        uppercase.
 *
 *  You then need to go through each function and modify according to the
 *  requirements of your provider's API.
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_INDAVIDEO_MAIN_URL', 'http://indavideo.hu/');

/**
 *  This is the URL to the API of your provider, if this exists.
 */
define('EMVIDEO_INDAVIDEO_API_URL', 'http://www.indavideo.hu/oembed');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_indavideo_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_INDAVIDEO_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_indavideo_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'indavideo',
    'name' => t('Indavideo'),
    'url' => EMVIDEO_INDAVIDEO_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !indavideo. You can also read more about its !api.', array('!indavideo' => l(t('indavideo.hu'), EMVIDEO_INDAVIDEO_MAIN_URL), '!api' => l(t("developer's API"), EMVIDEO_INDAVIDEO_API_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['indavideo'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_indavideo_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the indavideo provider.
  $form['indavideo']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the indavideo provider.
  $form['indavideo']['player_options']['emvideo_indavideo_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_indavideo_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_indavideo_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://www.indavideo.com/video/text-video-title
  // or embed code in the form of <object value="http://www.indavideo.com/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  return array(
    '@assets\.indavideo\.hu.*vID=([^\&]+)&@i',
    '@vID=([^\&]+)&.*assets\.indavideo\.hu@i',
    '@indavideo\.hu/player/video/([^"]+)@i',
    '@indavideo\.hu/video/([^"]+)@i',
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the indavideo provider.
 */
function emvideo_indavideo_data($field, $item) {
  // Initialize the data array.
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_indavideo_version'] = EMVIDEO_INDAVIDEO_DATA_VERSION;

  // We are using oEmbed to retrieve a standard set of data from the provider.
  // You should change the URL as specified by the indavideo provider.
  // If the indavideo provider does not support oEmbed, you must remove this
  // section entirely, or rewrite it to use their API.
  // See http://oembed.com/ for for information.
  $xml = emfield_request_xml('indavideo', 'http://indavideo.hu/oembed/'. $item['value'], array(), TRUE, FALSE, $item['value']);

  // This stores a URL to the video's thumbnail.
  $data['thumbnail'] = $xml['OEMBED']['THUMBNAIL_URL'][0];
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_indavideo_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_indavideo_embedded_link($video_code) {
  return 'http://www.indavideo.hu/video/'. $video_code;
}

/**
 * The embedded flash displaying the indavideo video.
 */
function theme_emvideo_indavideo_flash($item, $width, $height, $autoplay) {
  
  $output = '';
  if ($item['embed']) {
    $vid = $item['value'];
    $autoplay = $autoplay ? '1' : '0';
    $fullscreen = variable_get('emvideo_indavideo_full_screen', 1) ? 'true' : 'false';
    $output = '<object  classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,40,0" width="'. $width .'" height="'. $height .'" id="vplayer-'. $vid .'">
    <param name="src" value="http://assets.indavideo.hu/swf/player.swf" />
    <param name="menu" value="false"/>
    <param name="quality" value="high"/>
    <param name="bgcolor" value="#000000"/>
    <param name="wmode" value="transparent"/>
    <param name="allowscriptaccess" value="always"/>
    <param name="allowfullscreen" value="'. $fullscreen .'"/>
    <param name="flashvars" value="vID='. $vid .'&v=0084&onsite=1&autostart='. $autoplay .'" />
    <embed height="'. $height .'" width="'. $width .'" flashvars="vID='. $vid .'&v=1063&onsite=1&autostart='. $autoplay .'" allowfullscreen="'. $fullscreen .'" allowscriptaccess="always" wmode="transparent" bgcolor="#000000" quality="high" menu="false" src="http://assets.indavideo.hu/swf/player.swf" id="vplayer-'. $vid .'" type="application/x-shockwave-flash"/>
    </object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_indavideo_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $data['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_indavideo_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_indavideo_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_indavideo_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_indavideo_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_indavideo_emfield_subtheme() {
    $themes = array(
        'emvideo_indavideo_flash'  => array(
            'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
            'file' => 'providers/indavideo.inc',
            // If you don't provide a 'path' value, then it will default to
            // the emvideo.module path. Obviously, replace 'emindavideo' with
            // the actual name of your custom module.
            'path' => drupal_get_path('module', 'emindavideo'),
        )
    );
    return $themes;
}
